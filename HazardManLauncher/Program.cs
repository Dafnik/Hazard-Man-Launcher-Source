﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Threading;

namespace HazardManLauncher
{
    class Program
    {
        static string url = Links.versionlink; //Not useable without link
        static string downloadurl = Links.exelink; //Not useable without link
        static string optionspath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\HazardMan\\options";
        static string versionpath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\HazardMan\\versions";
        static string oldversion = "THIS_ISNT_REAL";
        static string newversion;
        static string optionsname = "\\launcher_config.txt";
        static string versionname = "\\HazardMan.exe";
        static bool loop = true;
        static bool gamestarted = false;
        static WebClient webclient;

        static void Main(string[] args)
        {
            while (loop)
            {
                createDirectory();

                createConfig();

                getVersion();

                checkVersion();
            }
        }

        static void createDirectory()
        {
            if (!Directory.Exists(versionpath))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\HazardMan\\versions");
                Console.WriteLine("[INFO] Created Directory versions");
            }
            else
            {
                Console.WriteLine("[INFO] Direcotry versions is available");
            }
            if (!Directory.Exists(optionspath))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\HazardMan\\options");
            }
            else
            {
                Console.WriteLine("[INFO] Direcotry options is available");
            }
        }

        static void createConfig()
        {
            if (!File.Exists(optionspath + optionsname))
            {
                Console.WriteLine("[INFO] Launcher config does not exist");
                Console.WriteLine("[INFO] Generating launcher options...");

                string[] lines = { oldversion };

                using (StreamWriter outputFile = File.CreateText(optionspath + optionsname))
                {
                    foreach (string line in lines)
                    {
                        outputFile.WriteLine(line);
                    }
                }

                Console.WriteLine("[INFO] Launcher config generated!");
            }
            else
            {
                Console.WriteLine("[INFO] Reading launcher config...");

                try
                {
                    string[] lines = File.ReadAllLines(optionspath + optionsname);

                    oldversion = lines[0];

                    Console.WriteLine("[INFO] Launcher config has been read successful");
                }
                catch
                {
                    Console.WriteLine("[ERROR] Launcher config is damaged!");
                    Console.WriteLine("[INFO] Deleting launcher config...");
                    File.Delete(optionspath + optionsname);
                    Console.WriteLine("[INFO] Launcher config has been deleted");
                    Console.WriteLine("[INFO] Starting creation again...");
                    createConfig();
                }
            }
        }

        static void getVersion()
        {
            webclient = new WebClient();
            Console.WriteLine("[INFO] Created WebClient");
            Console.WriteLine("[INFO] Getting current version...");
            try
            {
                newversion = webclient.DownloadString(url);
            }
            catch
            {
                Console.WriteLine("[ERROR] Getting current version FAILED!");
                loop = false;
                executeProgram();
            }
        }

        static void checkVersion()
        {
            if (newversion != oldversion)
            {
                Console.WriteLine("[INFO] Connecting to server...");

                try
                {
                    Uri uri = new Uri(downloadurl);

                    Console.WriteLine("[INFO] Connected");
                }
                catch
                {
                    Console.WriteLine("[ERROR] Could not connect!");
                    loop = false;
                    executeProgram();
                }

                Console.WriteLine("[INFO] Starting download...");
                try
                {
                    Random random = new Random();
                    int rand = random.Next(10000);
                    string torename = versionpath + versionname + rand;
                    webclient.DownloadFile(downloadurl, torename);
                    Console.WriteLine("[INFO] Download completed");
                    if (File.Exists(versionpath + versionname))
                    {
                        File.Delete(versionpath + versionname);
                        Console.WriteLine("[INFO] Old version has been deleted");
                    }

                    Console.WriteLine("[INFO] Renaming new version...");
                    File.Move(torename, versionpath + versionname);
                    Console.WriteLine("[INFO] New version renamed");

                }
                catch
                {
                    Console.WriteLine("[ERROR] Download FAILED!");
                    loop = false;
                    executeProgram();
                }

                Console.WriteLine("[INFO] Adding new version into launcher config...");
                string[] lines = { newversion };

                using (StreamWriter outputFile = File.CreateText(optionspath + optionsname))
                {
                    foreach (string line in lines)
                    {
                        outputFile.WriteLine(line);
                    }
                }

                Console.WriteLine("[INFO] New version has been successful added to launcher config");

                executeProgram();
            }
            else
            {
                Console.WriteLine("[INFO] You are running the newest version");
                executeProgram();
            }
        }

        static void executeProgram()
        {
            Console.WriteLine("[INFO] Checking if HazardMan is installed...");

            if (File.Exists(versionpath + versionname))
            {
                Console.WriteLine("[INFO] Running HazardMan...");
                if(!gamestarted) Process.Start(versionpath + versionname);
                gamestarted = true;
                loop = false;
            }
            else
            {
                Console.WriteLine("[ERROR] No HazardMan version to run! Can't download HazardMan!");
                Console.WriteLine("[INFO] Press any key to restart the process...");
                Console.ReadKey();
            }

        }
    }
}
